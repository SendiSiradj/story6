from django.shortcuts import render
from .models import Post
from .forms import PostForm
from django.http import HttpResponseRedirect


def index(request) :
    data = Post.objects.all()

    form = PostForm(None)
    dictio = {
        'forms':form,
        "test":"Hello",
        'data' : data
    }

    if request.method == "POST" :
        f = PostForm(request.POST)
        if f.is_valid():
            f.save()
            return HttpResponseRedirect('/test/')
    else:
        print("Masuk Else ======")
        return render(request, 'index.html', dictio)



# Create your views here.
