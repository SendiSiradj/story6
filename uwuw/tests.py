from django.test import TestCase
from django.test import Client
from django.http import HttpRequest
from .views import index
from django.urls import resolve


class UnitTest(TestCase) :

    # def test_hello_name_is_existed(self) :
    #     response = Client().get('/')
    #     print(response)
    #     self.assertEqual(response.status_code, 200)
    #
    # def test_page_function(self) :
    #     found = resolve('/')
    #     self.assertEqual(found.func, index)

    def test_add_post(self):
        response = Client().post('/test/', {"post":"abcdefg"})
        self.assertEqual(response.status_code, 302)

        response = Client().get('/test/')
        html_response = response.content.decode('utf8')
        self.assertIn("abcdefg", html_response)



# Create your tests here.
