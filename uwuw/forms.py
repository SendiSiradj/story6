from django import forms
from .models import Post
class PostForm(forms.ModelForm) :
    post = forms.CharField (max_length = 120)

    class Meta:
        model = Post
        fields = "__all__"
